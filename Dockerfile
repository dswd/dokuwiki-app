FROM cloudron/base:0.10.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

RUN curl -L http://download.dokuwiki.org/src/dokuwiki/dokuwiki-2017-02-19b.tgz | tar -xz --strip-components 1 -f -

ADD acl.auth.php /app/code/conf/acl.auth.php
ADD local.php /app/code/conf/local.php
ADD local.protected.php.template /app/code/local.protected.php.template
ADD mime.local.conf /app/code/conf/mime.local.conf
ADD .htaccess /app/code/.htaccess

# https://www.dokuwiki.org/config
RUN mv /app/code/conf /app/code/conf.orig && \
    mv /app/code/lib/tpl /app/code/lib/tpl.orig && \
    mv /app/code/lib/plugins /app/code/lib/plugins.orig && \
    ln -s /app/data/templates /app/code/lib/tpl && \
    ln -s /app/data/plugins /app/code/lib/plugins && \
    ln -s /app/data/conf /app/code/conf

RUN mkdir -p /run/dokuwiki/sessions

# configuring mail sending
RUN mkdir /app/code/lib/plugins.orig/smtp && \
    curl -L https://github.com/splitbrain/dokuwiki-plugin-smtp/tarball/103dc5610b48fb2afc2748c19183ad4974899259 | tar -xz --strip-components 1 -C /app/code/lib/plugins.orig/smtp/ -f -

RUN chown -R www-data.www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache2-dokuwiki.conf /etc/apache2/sites-available/dokuwiki.conf
RUN ln -sf /etc/apache2/sites-available/dokuwiki.conf /etc/apache2/sites-enabled/dokuwiki.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod php7.0 
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/dokuwiki/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100


# configuring rewrite
RUN a2enmod rewrite

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
