# Dokuwiki Cloudron App

This repository contains the Cloudron app package source for [Dokuwiki](https://www.dokuwiki.org/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.dokuwiki.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.dokuwiki.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd dokuwiki-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, perform tests, backup, restore and test if the pages are still ok.

```
cd dokuwiki-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## Debugging

Set https://www.dokuwiki.org/config:allowdebug to true
Set the ldap debug to true in local.protected.php

Use the debug functions in https://www.dokuwiki.org/devel:debugging 

Note that DokuWiki does not create any log files; use msg() to show debug in the rendered pages.

