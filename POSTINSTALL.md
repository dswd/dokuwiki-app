### Admin status

When using Cloudron authentication, any admin on Cloudron automatically
becomes a dokuwiki admin.

When not using Cloudron authentication, first register a new user. Then
edit `/app/data/conf/users.auth.php` and add the `admins` group to the new
user. You can disable registration by adding `$conf['disableactions']='register';`
to `/app/data/conf/local.php`. You can also disable dokuwiki's user management
by setting `useacl` to 0 in `/app/data/conf/local.php`.

