#!/bin/bash

set -eu

if ! [ -e "/app/data/data" ]; then
    echo "Initializing data directory on first run"
    cp -r /app/code/data/. /app/data/data
fi

mkdir -p /app/data/conf /app/data/templates /app/data/plugins

# Create 'regular' conf files so that they are immediately writable
cp -u /app/code/conf.orig/* /app/data/conf/

for f in $(ls /app/code/lib/tpl.orig); do
    if ! [ -e "/app/data/templates/$f" ]; then
        ln -s "/app/code/lib/tpl.orig/$f" "/app/data/templates/$f"
    fi
done

for f in $(ls /app/code/lib/plugins.orig); do
    if ! [ -e "/app/data/plugins/$f" ]; then
        ln -s "/app/code/lib/plugins.orig/$f" "/app/data/plugins/$f"
    fi
done


# https://www.dokuwiki.org/plugin:config#protecting_settings
sed -e "s|##MAIL_SMTP_SERVER|${MAIL_SMTP_SERVER}|" \
    -e "s|##MAIL_SMTP_PORT|${MAIL_SMTP_PORT}|" \
    -e "s|##MAIL_DOMAIN|${MAIL_DOMAIN}|" \
    -e "s|##MAIL_FROM|${MAIL_FROM}|" \
    -e "s|##MAIL_SMTP_USERNAME|${MAIL_SMTP_USERNAME}|" \
    -e "s|##MAIL_SMTP_PASSWORD|${MAIL_SMTP_PASSWORD}|" \
    /app/code/local.protected.php.template > /app/data/conf/local.protected.php


if [[ -n "${LDAP_SERVER:-}" ]]; then
    echo "Setting up LDAP"

    sed -e "s|##LDAP_URL|${LDAP_URL}|" \
        -e "s|##LDAP_USERS_BASE_DN|${LDAP_USERS_BASE_DN}|" \
        -e "s|##LDAP_BIND_DN|${LDAP_BIND_DN}|" \
        -e "s|##LDAP_BIND_PASSWORD|${LDAP_BIND_PASSWORD}|" \
        -i /app/data/conf/local.protected.php

    # putting this in protected file ensures user cannot change it in UI
    echo -e "\n\$conf['authtype']    = 'authldap';\n" >> /app/data/conf/local.protected.php
elif [[ ! -f /app/data/conf/users.auth.php ]]; then
    cp /app/data/conf/users.auth.php.dist /app/data/conf/users.auth.php
fi

chown -R www-data:www-data /app/data /run/dokuwiki

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
