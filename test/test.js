#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    mkdirp = require('mkdirp'),
    path = require('path'),
    rimraf = require('rimraf'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Key = require('selenium-webdriver').Key,
    Builder = require('selenium-webdriver').Builder;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);
    var server, browser = new webdriver.Builder().forBrowser('chrome').build();

    var LOCATION = 'test';
    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var email, token;

    function login(done) {
        browser.get('https://' + app.fqdn + '/start?do=login');
        browser.findElement(by.xpath('//input[@name="u"]')).sendKeys(username);
        browser.findElement(by.xpath('//input[@type="password"]')).sendKeys(password);
        browser.findElement(by.xpath('//input[@type="password"]')).sendKeys(Key.RETURN); // form submit does not work

        browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/start';
            });
        }, 400000).then(function () { done(); });
    }

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            token = result.body.token;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                email = result.body.email;
                done();
            });
        });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can get the main page', function (done) {
        superagent.get('https://' + app.fqdn).end(function (error, result) {
            expect(error).to.be(null);
            expect(result.status).to.eql(200);

            done();
        });
    });

    it('can login', login);

    it('logged into wiki', function (done) {
        browser.findElement(by.xpath('//*[contains(text(), "Logged in as:")]')).then(function () { done(); });
    });

    it('logged user as admin', function (done) {
        browser.findElement(by.xpath('//a[text()="Admin"]')).then(function () { done(); });
    });

    it('can edit page', function (done) {
        browser.get('https://' + app.fqdn + '/start&do=edit');

        browser.wait(until.elementLocated(by.xpath('//textarea')), 4000);
        browser.findElement(by.xpath('//textarea')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        browser.findElement(by.xpath('//textarea')).sendKeys('hello cloudron');

        browser.findElement(by.xpath('//button[text()="Save"]')).click();
        browser.wait(function () {
            return browser.getCurrentUrl().then(function (url) {
                return url === 'https://' + app.fqdn + '/start';
            });
        }, 4000).then(function () { done(); });
    });

    it('can upload a file', function (done) {
        browser.get('https://' + app.fqdn + '/start&do=media&ns=');

        browser.findElement(by.xpath('//a[text()="Upload"]')).click();
        browser.wait(until.elementLocated(by.xpath('//input[@type="file" and @name="file"]')), 4000);

        browser.findElement(by.xpath('//input[@type="file" and @name="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        browser.findElement(by.xpath('//button[@id="mediamanager__upload_button"]')).click();

        browser.wait(until.elementLocated(by.xpath('//a[text()="logo.png"]')), 4000).then(function () { done(); });
    });

    it('can restart app', function (done) {
        execSync('cloudron restart');
        done();
    });

    it('can login', function (done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.xpath('//*[contains(text(), "Logged in as:")]')).then(function () { done(); });
    });

    it('can see existing page', function (done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.xpath('//*[contains(text(), "hello cloudron")]')).then(function () { done(); });
    });

    it('can see uploaded image', function (done) {
        browser.get('https://' + app.fqdn + '/lib/exe/fetch.php?media=logo.png');
        browser.findElement(by.xpath('//img[contains(@src, "logo.png")]')).then(function () { done(); });
    });

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can see existing page', function (done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.xpath('//*[contains(text(), "hello cloudron")]')).then(function () { done(); });
    });

    it('can see uploaded image', function (done) {
        browser.get('https://' + app.fqdn + '/lib/exe/fetch.php?media=logo.png');
        browser.findElement(by.xpath('//img[contains(@src, "logo.png")]')).then(function () { done(); });
    });

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);

    it('logged user as admin', function (done) {
        browser.findElement(by.xpath('//a[text()="Admin"]')).then(function () { done(); });
    });

    it('can see existing page', function (done) {
        browser.get('https://' + app.fqdn);
        browser.findElement(by.xpath('//*[contains(text(), "hello cloudron")]')).then(function () { done(); });
    });

    it('can see uploaded image', function (done) {
        browser.get('https://' + app.fqdn + '/lib/exe/fetch.php?media=logo.png');
        browser.findElement(by.xpath('//img[contains(@src, "logo.png")]')).then(function () { done(); });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // check if the _first_ login via email succeeds
    it('can login via email', function (done) {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');

        login(function (error) {
            if (error) return done(error);

            execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
            done();
        });
    });

    it('install app without sso', function () {
        execSync('cloudron install --new --wait --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });

        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can see register link', function (done) {
        browser.get('https://' + app.fqdn + '/');
        browser.findElement(by.xpath('//a[contains(text(), "Register")]')).then(function () { done(); });
    });

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
